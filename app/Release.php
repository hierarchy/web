<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Release extends Model
{
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'releases';

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'version', 'link', 'zip', 'user'
    ];
}