<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
* 
*/
class UserToken extends Model
{
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users_token';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'domain',
        'token',
        'access_count',
        'expire_token',
    ];

    public function owner()
    {
        return $this->belongsTo(\App\User::class, 'user_id');
    }

    public static function findByToken($token)
    {
        return static::where('token', $token)->first();
    }


}