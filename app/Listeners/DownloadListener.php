<?php

namespace App\Listeners;

use App\Events\DownloadEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use GuzzleHttp\Client;

class DownloadListener implements ShouldQueue
{   
    use InteractsWithQueue;

    /**
     * [$http description]
     * @var [type]
     */
    protected $http;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Client $http)
    {
        $this->http = $http;
    }

    /**
     * Handle the event.
     *
     * @param  DownloadEvent  $event
     * @return void
     */
    public function handle(DownloadEvent $event)
    {
        return $this->download($event->releases);
    }

    /**
     * [download description]
     * @param  [type] $release [description]
     * @return [type]          [description]
     */
    public function download($release)
    {
        if(is_null($release->zip)){
            throw new \Exception("Error Processing Request", 1);
        }

        $version = time().'v.'.$release->version.'.zip';
        $release->filename = $version;
        $release->save();

        try {
            $resource = fopen(storage_path('archives'). '/'. $version, 'w');
            $this->http->request('GET', $release->zip , ['sink' => $resource, 'auth'=>['hierarchy.id@gmail.com','c0m3nk1981']]);
        } catch (\Exception $e) {
            return $e->getMessage();
        }        
    }
}
