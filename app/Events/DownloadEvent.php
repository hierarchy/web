<?php

namespace App\Events;

use App\Release;

class DownloadEvent extends Event
{	

	/**
	 * [$releases description]
	 * @var [type]
	 */
	public $releases;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Release $releases)
    {
        $this->releases = $releases;
    }
}
