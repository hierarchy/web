<?php
namespace App\Modules\Client\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class ClientServiceProvider extends ServiceProvider
{	

	/**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('client::setting.*', function ($view) {
            $view->with('menu', config('client.menu'));
        });
    }

	/**
	 * Register the Client module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\Client\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the Client module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('client', realpath(__DIR__.'/../Resources/Lang'));

		View::addNamespace('client', base_path('resources/views/vendor/client'));
		View::addNamespace('client', realpath(__DIR__.'/../Resources/Views'));
	}
}
