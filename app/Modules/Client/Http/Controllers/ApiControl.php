<?php

namespace App\Modules\Client\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\UserToken;
use Auth;
use Illuminate\Http\Request;

class ApiControl extends Controller
{	
	protected $usertokens;

	public function __construct(UserToken $usertokens)
	{
		$this->usertokens = $usertokens;
	}

	public function index()
	{
		return view('client::setting.api.index');
	}

	public function generate(Request $request)
	{
		$this->validate($request, [
			'name' => 'required',
			'domain' => 'url|active_url|unique:users_token,domain'
		]);
		
		$row = $this->usertokens;
		$row->fill($request->all());
		$row->token = str_random(60);
		$row->user_id = auth()->user()->id;
		$row->save();

		return redirect()->to('client/api');
	}

	public function check()
	{
		return auth()->user()->toArray();
	}
}
