<?php
namespace App\Modules\Client\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfileControl extends Controller
{   
    public function index()
    {   
        config(['client.menu' => 'profile']);
        $user = Auth::user();
        return view('client::setting.profile.index', compact('user'));
    }

    public function updateProfile(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255'
        ]);

        $user = Auth::user();
        $user->name = $request->name;
        $user->save();

        return redirect('client');
    }

    /**
     * [update description]
     * @return [type] [description]
     */
	public function update()
	{      
        config(['client.menu' => '']);
		return view('client::setting.security.update');
	}

	/**
     * [updateProfile description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function completeProfile(Request $request)
    {   
        $this->validate($request, [
            'username' => 'required|max:50|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);

        $user = Auth::user();
        $user->username = $request->username;
        $user->password = bcrypt($request->password);
        $user->active = true;
        if($user->save()){
            Auth::logout();
            return redirect('login');
        }
        
        return redirect('client');
    }

    public function password()
    {   
        config(['client.menu' => 'password']);
        return view('client::setting.security.index');
    }


    public function updatePassword(Request $request)
    {
        $this->validate($request, [
            'old_password' => 'required|hashmatch:' . auth()->user()->id,
            'password'     => 'required|confirmed',
        ]);

        $user = auth()->user();
        $user->password = bcrypt($request->password);
        $user->save();

        return redirect()->to('client');
    }
}
