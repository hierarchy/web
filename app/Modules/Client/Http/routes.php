<?php


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/



Route::group(['prefix' => 'client', 'middleware' => ['web', 'auth']], function () {
	
	Route::get('/', function() {
		return view('client::home');
	});
	
	Route::get('profile', 'ProfileControl@index');
	Route::post('profile', 'ProfileControl@updateProfile');
	Route::get('update', 'ProfileControl@update');
	Route::post('update', 'ProfileControl@completeProfile');
	Route::get('password', 'ProfileControl@password');
	Route::post('password', 'ProfileControl@updatePassword');

	Route::get('api', 'ApiControl@index');
	Route::post('api/generate', 'ApiControl@generate');

});

Route::group(['prefix' => 'client', 'middleware' => 'api'],function(){
	Route::get('coba', 'ApiControl@check');
});
