<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['prefix' => 'docs'], function() {
	Route::get('/', function() {
		return view('docs::v1.installation');
	});

	Route::get('installation', ['as' => 'installation', function(){
		return view('docs::v1.installation');
	}]);

	Route::get('quickstart', ['as' => 'quickstart', function(){
		return view('docs::v1.quickstart');
	}]);
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['prefix' => 'docs', 'middleware' => ['web']], function () {
	//
});
