<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash;
use Validator;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'avatar', 'username',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_admin' => 'boolean',
        'active' => 'boolean'
    ];

    /**
     * [scopeFindOrCreate description]
     * @param  [type] $query [description]
     * @param  [type] $id    [description]
     * @return [type]        [description]
     */
    public function scopeFindOrCreate($query, $email)
    {
        $obj = $query->where('email', $email)->first();
        return $obj ?: new static;
    }

    /**
     * [tokens description]
     * @return [type] [description]
     */
    public function tokens()
    {
        return $this->hasMany(\App\UserToken::class, 'user_id');
    }

    /**
    * Bootstrap any application services.
    *
    * @return void
    */
    static function boot()
    {
        // Check given password against password stored in database
        Validator::extend('hashmatch', function($attribute, $value, $parameters)
        {
            $user = User::find($parameters[0]);

            return Hash::check($value, $user->password);
        });

        // Check that given password is not the same as the password stored in database
        // Validator::extend('hashdiffer', function($attribute, $value, $parameters)
        // {
        //     $user = User::find($parameters[0]);

        //     return ! Hash::check($value, $user->password);
        // });
    }
}
