<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Socialite;
use Validator;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = 'client';

    /**
     * [$oauthDriver description]
     * @var string
     */
    protected $oauthDriver = 'google';
    
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {   
        $this->middleware($this->guestMiddleware(), ['except' => ['logout', 'updateProfile']]);        
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'username' => 'required|max:50|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'username' => $data['username'],
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => (isset($data['password'])) ? bcrypt($data['password']) : null,
            'avatar' => $data['avatar'],
        ]);
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver($this->oauthDriver)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {
        $oauthUser = Socialite::driver($this->oauthDriver)->user();
        $token = $oauthUser->token;
        
        $user = User::FindOrCreate($oauthUser->getEmail());
        $user->username = $oauthUser->getNickname();
        $user->name = $oauthUser->getName();
        $user->email = $oauthUser->getEmail();
        $user->avatar = $oauthUser->getAvatar();
        $user->save();

        Auth::login($user);

        if( $user && !is_null($user->password) && $user->active){
            return redirect('client');
        }

        return redirect('client/update');       
    }
}
