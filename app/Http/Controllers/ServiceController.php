<?php

namespace App\Http\Controllers;

use App\Events\DownloadEvent;
use App\Http\Controllers\Controller;
use App\Release;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Log;

class ServiceController extends Controller
{
	/**
	 * [$releases description]
	 * @var [type]
	 */
	protected $releases;

    /**
     * [$http description]
     * @var [type]
     */
    protected $http;

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Release $releases, Client $http)
    {
        $this->releases = $releases;
        $this->http = $http;
    }

    /**
     * [FunctionName description]
     * @param string $value [description]
     */
    public function hooks(Request $request)
    {
    	$payload = $request->all();

    	$informations = []; 
    	if(array_key_exists('push', $payload)){
    		foreach ($payload['push']['changes'] as $key => $value) {
    		 	if(array_key_exists('new', $value)){
    		 		if($value['new']['type'] == 'tag'){
    		 			$informations['version'] =  $value['new']['name'];
    		 		}else{
                        return response()->json('Not tag push', 200);
                    }
    		 	}
    		}
    	}
    	if(array_key_exists('repository', $payload)){
    		$informations['link'] = $payload['repository']['links']['html']['href'];
    		$informations['name'] = $payload['repository']['full_name'];
    	}

    	$informations['user'] = $payload['actor']['display_name'];
    	$informations['zip'] = $informations['link'].'/get/'.$informations['version'].'.zip';

    	$release = $this->releases;
    	$release->fill($informations);
    	if($release->save()){
            event(new DownloadEvent($release));    		
            return response()->json('success add releases', 200);
    	}

    	return response()->json('error add releases', 200);
    }

    public function releases()
    {
    	return $this->releases->all()->toArray();
    }

    public function download($vendor, $repository)
    {
        $repository_name = $vendor.'/'.$repository;
        $version = $this->releases->where('name', $repository_name)->latest()->first();

        $filename = $version->filename;

        return response()->download(storage_path('archives')."/{$filename}", 'latest.zip');

    }
}