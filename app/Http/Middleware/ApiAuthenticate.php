<?php

namespace App\Http\Middleware;

use App\UserToken;
use Closure;
use Illuminate\Support\Facades\Auth;

class ApiAuthenticate
{   
    /**
     * [$inputKey description]
     * @var string
     */
    protected $inputKey = 'api_token';

    /**
     * [$usertokens description]
     * @var [type]
     */
    protected $usertokens;
    
    /**
     * [__construct description]
     * @param UserToken $usertokens [description]
     */
    public function __construct(UserToken $usertokens)
    {
        $this->usertokens = $usertokens;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {  
        if (! $this->tokenCheck($request)) {
            if ($request->ajax() || $request->wantsJson() || $request->isJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('login');
            }
        }

        return $next($request);
    }

    public function tokenCheck($request)
    {
        $token = $this->getTokenForRequest($request);
        if($userToken = $this->usertokens->findByToken($token)){
            $userToken->access_count = $userToken->access_count+1;
            $userToken->save();
            Auth::login($userToken->owner);
            return true;
        }
        return false;
    }

    /**
     * Get the token for the current request.
     *
     * @return string
     */
    protected function getTokenForRequest($request)
    {
        $token = $request->input($this->inputKey);

        if (empty($token)) {
            $token = $request->bearerToken();
        }

        if (empty($token)) {
            $token = $request->getPassword();
        }

        return $token;
    }
}
