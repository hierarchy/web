<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
	
	

Route::group(['domain' => 'archive.hierarchy.id'], function () {
	Route::get('/', function(){
		return view('archive.main');
	});
	
    Route::post('/hooks', 'ServiceController@hooks');
	Route::get('/download/{vendor}/{repository}/latest.zip', 'ServiceController@download')->middleware('api');
});


Route::get('/', function () {
    return view('homes.main');
});

Route::get('/login', function(){
	return view('auth.login');
});

Route::auth();
Route::get('register', 'Auth\AuthController@redirectToProvider');
Route::get('oauth/login', 'Auth\AuthController@redirectToProvider');
Route::get('oauth/callback', 'Auth\AuthController@handleProviderCallback');

Route::get('/home', 'HomeController@index');

