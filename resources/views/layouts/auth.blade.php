<!DOCTYPE html>
<html lang="id">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>HiERARCHY WEB</title>

		<!-- Load CSS -->
		<link rel="shortcut icon" href="{{ asset('assets/img/favicon.ico') }}" type="image/x-icon">
		<link rel="icon" href="{{ asset('assets/img/favicon.ico') }}" type="image/x-icon">
		
		<link rel="stylesheet" href="{{ asset('assets/css/textmeone.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/semantic.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/auth.css') }}">
	</head>

	<body>
		<div class="pusher">
			@yield('hibody')
		</div>
		
		<!-- Load JS -->
		<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
		@yield('hiscript')
	</body>
</html>