<div class="slide">
	<div class="ui container">
		<div class="ui two column grid">
			<div class="row">
				<div class="column">
					@yield('slideLeft')
				</div>

				<div class="column">
					@yield('slideRight')
				</div>	
			</div>
		</div>
	</div>
</div>