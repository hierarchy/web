<!DOCTYPE html>
<html lang="id">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>HiERARCHY WEB</title>

		<!-- Load CSS -->
		<link rel="shortcut icon" href="{{ asset('assets/img/favicon.ico') }}" type="image/x-icon">
		<link rel="icon" href="{{ asset('assets/img/favicon.ico') }}" type="image/x-icon">

		<link rel="stylesheet" href="{{ asset('assets/css/textmeone.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/sanspro.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/semantic.min.css') }}">
	</head>

	<style>
		body {
			background: -moz-radial-gradient(center, ellipse cover, rgba(255,255,255,0.27) 0%, rgba(0,188,212,1) 100%); 
			background: -webkit-radial-gradient(center, ellipse cover, rgba(255,255,255,0.27) 0%,rgba(0,188,212,1) 100%);
			background: radial-gradient(ellipse at center, rgba(255,255,255,0.27) 0%,rgba(0,188,212,1) 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#45ffffff', endColorstr='#00bcd4',GradientType=1 );
		}

		body > .pusher {
			position: relative;
			display: flex;
			height: 100vh;
			width: 100vw;
			align-items: center;
		}

		.hierarchy,
		.hierarchy * {
		    display: block;
		    margin: 0 auto;
		}

		.hierarchy > img {
			width: 88px;
		}

		code.hierarchy-font {
			color: #00bcd4;
		    font-family: "Text Me One";
		    font-size: 2em;
		    margin-top: 0.5em;
		}
	</style>

	<body>
		<div class="pusher">
			<div class="hierarchy">
				<img src="{{ asset('assets/img/hijasa-logo.svg') }}" alt="">
				<code class="hierarchy-font">HiERARCHY</code>
			</div>
		</div>
	</body>
</html>