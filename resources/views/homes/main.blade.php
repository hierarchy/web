@extends('layouts.base')

@section('hibody')
<div class="ui pages">
	<div class="section">
		@include('homes.welcome.index')
	</div>
	
	<div class="section">
		@include('homes.fiture.index')
	</div>

	<div class="section">
		@include('homes.why.index')
	</div>
	
	<!-- ...others -->
</div>
@endsection

@section('hiscript')
<script>
$(document).ready(function() {
	$('.ui.pages').fullpage({
		// slidesNavigation: true
	});
});
</script>
@endsection