<div class="slide">
	<div class="ui container">
		<div class="ui two column grid">
			<div class="row">
				<div class="column">
					<h2 class="ui header">Easy Statistic Chart</h2>
					<p>Cukup siapkan file excel yang sederhana terdiri dari baris dan kolom tanpa merging cell, 
					upload ke <code class="hierarchy-font">HiCMS</code> maka akan dikonversi menjadi <code>chart/graph</code> yang indah. warna dari chart bisa di generate secara acak dan pilih yang sesuai. Bentuk chart apakah pie, bar, line 
					bisa dipilih sesuai karakteristik dari data yang akan ditampilkan.</p>
				</div>

				<div class="column">
					<img src="{{ asset('assets/img/chart.png') }}">
				</div>	

				<div class="column">
					@yield('slideRight')
				</div>	
			</div>
		</div>
	</div>
</div>