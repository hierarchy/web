<div class="slide">
	<div class="ui container">
		<div class="ui two column grid">
			<div class="row">
				<div class="column">
					<h2 class="ui header">Package Support</h2>
					<p>Beragam <code>Paket Aplikasi</code> lain yang dikembangkan oleh vendor yang sudah tergabung dalam <code class="hierarchy-font">HiERARCHY</code> bisa dipilih oleh user sesuai kebutuhan. Anda bayangkan paket-paket aplikasi tersebut semacam 'Play-Store' -versi hierarchy- yang berisi banyak aplikasi yang bisa anda gunakan. </p>
				</div>

				<div class="column">
					<img src="{{ asset('assets/img/box.png') }}">
				</div>

				<div class="column">
					@yield('slideRight')
				</div>	
			</div>
		</div>
	</div>
</div>