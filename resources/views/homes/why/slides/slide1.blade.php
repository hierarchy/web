<div class="slide">
	<div class="ui container">
		<div class="ui two column grid">
			<div class="row">
				<div class="column">
					<h2 class="ui header">Markdown Editor</h2>
					<p>Konten teks (Heading, Bullet and Numbering, Bold, Italic), tabel, gambar, external link, video, dapat dengan mudah kita buat dengan fitur <code>Markdown Editor</code>. Layout halaman dari Markdown Editor di rancang agar tampil secara side-by-side, sehingga memudahkan kita melihat hasil akhir dari konten/halaman web yang sedang dibuat.</p>
				</div>

				<div class="column">
					<img src="{{ asset('assets/img/markdown.png') }}">
				</div>	
			</div>
		</div>
	</div>
</div>