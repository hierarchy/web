<div class="slide">
	<div class="ui container">
		<div class="ui two column grid">
			<div class="row">
				<div class="column">
					<h2 class="ui header">Jabatan Struktural</h2>
					<p>Setiap organisasi tentu memiliki struktur organisasi dan terdapat jabatan struktural yang secara jelas ada pada struktur organisasi tersebut. Pengelolaan data pejabat di <code class="hierarchy-font">HiCMS</code> sudah mengikuti pola Eselonisasi, Kepangkatan dan Jabatannya. Jadi... cukup siapkan identitas pejabat seperti Foto, Nama, NIP, Pangkat, Jabatan, Eselon dan tupoksi dari setiap jabatan struktural yang ada di organisasi tersebut lalu input dengan mudah dan sederhana di <code class="hierarchy-font">HiCMS</code> , pun data pejabat struktural tersebut akan terintegrasi dengan website organisasi diatasnya.</p>
				</div>

				<div class="column">
					<img src="{{ asset('assets/img/struktural.png') }}">
				</div>

				<div class="column">
					@yield('slideRight')
				</div>	
			</div>
		</div>
	</div>
</div>