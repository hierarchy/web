<div class="slide">
	<div class="ui container">
		<div class="ui two column grid">
			<div class="row">
				<div class="column">
					<h2 class="ui header">Integrasi Dokumen</h2>
					<p>Peraturan, Perundang-undangan, juknis atau dokumen sejenis yang ingin dipublikasikan oleh instansi bisa dikelola dengan mudah. Dan secara hirarki dokumen-dokumen tersebut dapat diakses oleh instansi induk diatasnya dengan fitur integrasi menjadi sebuah direktori dokumen. Fitur pencarian berdasarkan kategori akan memudahkan pengunjung website untuk mencari file yang diingingkan. Semakin banyak dokumen yang diupload oleh instansi maka akan menjadi sebuah 'perpustakaan daerah' yang bermanfaat bagi pengunjung website.</p>
				</div>

				<div class="column">
					<img src="{{ asset('assets/img/peraturan.png') }}">
				</div>

				<div class="column">
					@yield('slideRight')
				</div>	
			</div>
		</div>
	</div>
</div>