<div class="slide">
	<div class="ui container">
		<div class="ui two column grid">
			<div class="row">
				<div class="column">
					<h2 class="ui header">Kontrol Berita</h2>
					<p><code class="hierarchy-font">HiCMS</code> membantu Anda untuk dapat mengontrol <code>Berita dan Komentar</code> baik di web instansi Anda maupun di web instansi yang menjadi <code>child-web</code> dengan sangat mudah. <code class="hierarchy-font">HiCMS</code> juga memungkinkan Anda untuk mengambil berita dari <code>child-web</code> hanya dengan <code>single-click</code> dan <code class="hierarchy-font">HiCMS</code> akan menampilkan berita tersebut di web instansi Anda secara otomatis. Opsi tagging, publish, date, commented, memudahkan bagaimana sebuah berita ditampilkan.</p>
				</div>

				<div class="column">
					<img src="{{ asset('assets/img/event.png') }}">
				</div>	

				<div class="column">
					@yield('slideRight')
				</div>	
			</div>
		</div>
	</div>
</div>