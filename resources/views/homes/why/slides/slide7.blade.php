<div class="slide">
	<div class="ui container">
		<div class="ui two column grid">
			<div class="row">
				<div class="column">
					<h2 class="ui header">Themes and Widgets</h2>
					<p>Tampilan web pada halaman utama merupakan halaman yang terdiri dari <code>theme dan widget</code>. News, Gallery, Statistic, Event, bahkan Addon-Packages, adalah contoh widget default yang tersedia di <code class="hierarchy-font">HiCMS</code>. Kita bisa mengatur apakah widget-widget tersebut akan diaktifkan atau tidak dengan maksud untuk memberikan kenyamanan maupun kemudahan para pembaca mendapatkan informasi dari halaman yang kita kelola.</p>
				</div>

				<div class="column">
					<img src="{{ asset('assets/img/themes1.png') }}">
				</div>

				<div class="column">
					@yield('slideRight')
				</div>	
			</div>
		</div>
	</div>
</div>