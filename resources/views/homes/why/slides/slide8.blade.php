<div class="slide">
	<div class="ui container">
		<div class="ui two column grid">
			<div class="row">
				<div class="column">
					<h2 class="ui header">API Ready</h2>
					<p>API adalah suatu cara untuk saling terhubung antar satu aplikasi dengan aplikasi yang lain sehingga informasi dibagi antara satu sama lain tanpa harus melalui proses penerjemahan atau modifikasi kode pemrograman ulang. Data-data berita, dokumen peraturan dan data pejabat struktural yang ada pada website SKPD berkomunikasi sehingga data-data tersebut bisa ditampilkan pada website Pemda diatasnya secara hirarki adalah salah satu fitur <code class="hierarchy-font">HiCMS</code> dalam hal integrasi data melalui teknologi <code>API</code>.</p>
				</div>
				<div class="column">
					<img src="{{ asset('assets/img/api.png') }}">
				</div>

				<div class="column">
					@yield('slideRight')
				</div>	
			</div>
		</div>
	</div>
</div>