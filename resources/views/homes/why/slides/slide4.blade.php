<div class="slide">
	<div class="ui container">
		<div class="ui two column grid">
			<div class="row">
				<div class="column">
					<h2 class="ui header">Media Support</h2>
					<p>Pengelolaan terhadap semua dokumen (photo, file, chart, video, dsb) yang diperlukan dalam pembuatan konten berita dapat dilakukan dengan mudah melalui fitur <code>Media Support</code>. Quick edit (rename, description) bisa ditambahkan untuk melengkapi informasi dari file tersebut. Upload dan delete file juga dengan mudah dilakukan dengan fitur ini.</p>
				</div>

				<div class="column">
					<img src="{{ asset('assets/img/media.png') }}">
				</div>	

				<div class="column">
					@yield('slideRight')
				</div>	
			</div>
		</div>
	</div>
</div>