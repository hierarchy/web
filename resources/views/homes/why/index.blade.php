<h1 class="ui header">
	<div class="ui container">
	Lebih Dekat Tentang <code class="hierarchy-font standart">HiCMS</code>
	</div>
</h1>

<div class="ui container">
@include('homes.why.slides.slide1')
@include('homes.why.slides.slide2')
@include('homes.why.slides.slide3')
@include('homes.why.slides.slide4')
@include('homes.why.slides.slide5')
@include('homes.why.slides.slide6')
@include('homes.why.slides.slide7')
@include('homes.why.slides.slide8')
@include('homes.why.slides.slide9')
</div>