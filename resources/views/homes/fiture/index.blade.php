<div class="ui container">
	<div class="ui divided grid">
		<div class="row">
			<div class="column">
				<h1 class="ui header">Mengapa Memilih <code class="hierarchy-font standart">HiCMS</code>?</h1>
			</div>
		</div>

		<div class="four column row">
			<div class="column">
				<h2 class="ui header">Hirarki dan Terintegrasi</h2>
				<p>Instansi pemerintah daerah setingkat Pemprov/Pemda, SKPD, UPT hingga unit organisasi dibawahnya terstruktur secara hirarki. <code class="hierarchy-font">HiCMS</code> dirancang untuk memenuhi kebutuhan website instansi dengan menambahkan fitur integrasi konten antar instansi tersebut.</p>
			</div>

			<div class="column">
				<h2 class="ui header">Standar/Custom Menu</h2>
				<p>Menu standar untuk kebutuhan instansi pemerintah seperti Profil, Visi, Misi, Personil Pejabat, sudah tersedia di
				<code class="hierarchy-font">HiCMS</code> secara default. Menu lainnya dapat ditambahkan secara mandiri sesuai kebutuhan instansi.</p>
			</div>

			<div class="column">
				<h2 class="ui header">Pengelolaan Berita</h2>
				<p>Sebuah berita dengan mudah dapat dibuat di <code class="hierarchy-font">HiCMS</code>. Opsi <code>quick edit, publish-post, pinned-post, commented-post </code> dan fitur <code>tagging</code> memudahkan kita mengatur karakteristik sebuah berita.
				</p>
			</div>
			
			<div class="column">
				<h2 class="ui header">Markdown Editor</h2>
				<p>Sebuah halaman web yang berisi <code>konten multimedia </code> (teks, gambar, tabel, bahkan video) dengan sangat mudah dan sederhana bisa dibuat dengan fitur utama yaitu <code>Markdown Editor.</code></p>
			</div>
		</div>

		<div class="four column row">
			<div class="column">
				<h2 class="ui header">Media Support</h2>
				<p>Akses dan pengelolaan terhadap semua dokumen (photo, file, chart, video, dsb) yang diperlukan dalam pembuatan konten berita dapat dilakukan dengan mudah melalui fitur <code>Media Support</code></p>
			</div>

			<div class="column">
				<h2 class="ui header">Multi Language</h2>
				<p>Kita tidak perlu lagi membuat banyak web untuk mengaplikasikan multi bahasa. HiCMS memberi kemudahan untuk menentukan konten mana yang ingin aktifkan untuk <code>multi bahasa.</code></p>
			</div>

			<div class="column">
				<h2 class="ui header">Fitur Lainnya...</h2>
				<p>Pengelolaan Banner, Agenda Kegiatan, Galery Photo, Layanan Download Dokumen, Notifikasi, Pengelolaan User, Optimasi Grafis, Easy Statistic, Addon-Package, ...</p>
			</div>

			<div class="column">
				<h2 class="ui header">...</h2>
				<p>Integrasi Dokumen, Widget, Themeing, Search Indexing, dan fitur hebat <code class="hierarchy-font">HiCMS</code> lainnya.</p>
			</div>
		</div>
	</div>
</div>