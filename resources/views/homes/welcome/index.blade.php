<div class="ui container">
	<div class="ui two column grid">
		<div class="row">
			<div class="column">
				<h1 class="ui header">HiCMS - The Simplest Government CMS</h1>
				
				<p>Layaknya sebuah Content Management System (CMS), <code class="hierarchy-font">HiCMS</code> adalah aplikasi yang digunakan untuk membuat, mengubah dan mempublikasikan konten ke dalam sebuah website. HiCMS lahir dengan maksud untuk memberikan kemudahan dan kenyamanan bagi lembaga pemerintah daerah dalam pengelolaan situs web yang terintegrasi secara hirarki. </p>
			</div>
			
			<div class="column">
				<img src="{{ asset('assets/img/alexa.png') }}">
			</div>
		</div>
	</div>
</div>