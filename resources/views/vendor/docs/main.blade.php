<!DOCTYPE html>
<html lang="id">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>HiERARCHY WEB</title>

		<!-- Load CSS -->
		<link rel="stylesheet" href="{{ asset('assets/css/sanspro.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/textmeone.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/semantic.min.css') }}">
		<link rel="stylesheet" href="{{ asset('vendor/client/css/offset.css') }}">
	</head>

	<body>
		<div class="ui top fixed borderless menu">
			<div class="ui container">
				<div class="item brand">
					<img src="{{ asset('assets/img/hijasa-logo.svg') }}" alt="">
					<code class="hierarchy-font">HiERARCHY</code>
				</div>
				
				<div class="right menu">
					@if (Auth::guest())
					<a href="{{ url('login') }}" class="item">LOGIN</a>
					<a href="{{ url('register') }}" class="item">REGISTER</a>
					@endif
				</div>
			</div>
		</div>

		<div class="pusher">
			@yield('hipage')
		</div>
		<!-- Load JS -->
		<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
		<script src="{{ asset('assets/js/semantic.min.js') }}"></script>
		<script>
			$(document).ready(function() {
				$('.ui.dropdown').dropdown();
			});
		</script>
	</body>
</html>