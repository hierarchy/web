<div class="ui fluid vertical menu">
	<div class="ui top attached item label">GETTING STARTED</div>
	<a href="{{ route('installation') }}" class="item">
		<i class="icon database"></i>
		Installation
	</a>

	<a href="{{ route('quickstart') }}" class="item">
		<i class="icon file text"></i>
		Quickstart
	</a>

	<a href="#" class="item">
		<i class="icon help circle"></i>
		F.A.Q
	</a>

	<a href="#" class="item">
		<i class="icon book"></i>
		Release Notes
	</a>
</div>

<div class="ui fluid vertical menu">
	<div class="ui top attached item label">SERVER</div>
	<a href="#" class="item">
		<i class="icon payment"></i>
		Payment Method
	</a>

	<a href="#" class="item">
		<i class="icon history"></i>
		Invoices
	</a>
</div>