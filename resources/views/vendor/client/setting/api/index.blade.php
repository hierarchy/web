@extends('client::setting.main')

@section('hicontent')

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="ui flat segment">
	<div class="ui top attached label">CREATE API TOKEN</div>
	<div class="ui text container">
		{{ Form::open(['url' => 'client/api/generate', 'class' => 'ui form']) }}
			<div class="inline fields">
				<div class="five wide field">
					<label for="">Token Name</label>
				</div>
				<div class="eleven wide field">
					{!! Form::text('name', null) !!}
				</div>
			</div>
			<div class="inline fields">
				<div class="five wide field">
					<label for="">Domain Name</label>
				</div>
				<div class="eleven wide field">
					{!! Form::text('domain', null) !!}
				</div>
			</div>
			<div class="inline fields">
				<div class="five wide field"></div>
				<div class="eleven wide field">
					<button class="ui teal button">CREATE</button>
				</div>
			</div>
		{!! Form::close() !!}
	</div>
</div>

<div class="ui flat segment">
	<div class="ui top attached label">API TOKENS</div>
	<!-- TABEL -->
	<table class="ui table">
		<thead>
			<tr>
				<th>NAME</th>
				<th>DOMAIN</th>
				<th>TOKEN</th>
				<th>REQUEST</th>
			</tr>
		</thead>
		@foreach(auth()->user()->tokens as $row)
		<tbody>
			<tr>
				<td>{{ $row->name }}</td>
				<td>{{ $row->domain }}</td>
				<td>{{ $row->token }}</td>
				<td>{{ $row->access_count }}</td>
			</tr>
		</tbody>
		@endforeach
	</table>
</div>
@endsection