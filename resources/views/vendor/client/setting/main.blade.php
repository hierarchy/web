@extends('client::main')

@section('hipage')
<div class="ui container">
	<div class="ui grid">
		<div class="row">
			<div class="four wide column">
				@include('client::setting.menu')
			</div>

			<div class="twelve wide column">
				@yield('hicontent')
			</div>
		</div>
	</div>
</div>
@endsection