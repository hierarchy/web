@extends('client::setting.main')

@section('hicontent')
<div class="ui flat segment">
	<div class="ui top attached label">PROFILE PHOTO</div>
	<div class="ui text container">
		<form action="" class="ui form">
			<div class="inline fields">
				<div class="five wide field"></div>
				<div class="eleven wide field">
					<div class="ui small image">
						<img src="{{ $user->avatar }}">
						<button class="ui teal fluid button">CHANGE PHOTO</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<div class="ui flat segment">
	<div class="ui top attached label">CONTACT INFORMATION</div>
	<div class="ui text container">
		<form action="{{ url('client/profile') }}" class="ui form" method="POST">
			{!! csrf_field() !!}
			<div class="inline fields">
				<div class="five wide field">
					<label for="">Full Name</label>	
				</div>
				<div class="eleven wide field">
					<input type="text" value="{{ $user->name }}" name="name">
				</div>
			</div>

			<div class="inline fields">
				<div class="five wide field">
					<label for="">E-Mail Address</label>
				</div>
				<div class="eleven wide field">
					<input type="text" value="{{ $user->email }}" readonly="true">
				</div>
			</div>

			<div class="inline fields">
				<div class="five wide field"></div>
				<div class="eleven wide field">
					<button class="ui teal button" type="submit">UPDATE</button>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection