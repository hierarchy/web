<div class="ui fluid vertical menu">
	<div class="ui top attached item label">SETTINGS</div>
	<a href="{{ url('client/profile') }}" class="item {{ $menu  == 'profile' ? 'active': '' }}">
		<i class="icon edit"></i>
		Profile
	</a>

	<a href="{{ url('client/password') }}" class="item {{ $menu  == 'password' ? 'active': '' }}">
		<i class="icon lock"></i>
		Security
	</a>

	<a href="{{ url('client/api') }}" class="item {{ $menu  == 'api' ? 'active': '' }}">
		<i class="icon cubes"></i>
		API
	</a>
</div>

<div class="ui fluid vertical menu">
	<div class="ui top attached item label">BILLINGS</div>
	<a href="#" class="item {{ $menu  == 'payment' ? 'active': '' }}">
		<i class="icon payment"></i>
		Payment Method
	</a>

	<a href="#" class="item {{ $menu  == 'invoice' ? 'active': '' }}">
		<i class="icon history"></i>
		Invoices
	</a>
</div>