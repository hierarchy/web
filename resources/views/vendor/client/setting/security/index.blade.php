@extends('client::setting.main')

@section('hicontent')

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="ui flat segment">
	<div class="ui top attached label">UPDATE PASSWORD</div>
	<div class="ui text container">
		<form action="{{ url('client/password') }}" class="ui form" method="POST">
			{!! csrf_field() !!}
			<div class="inline fields">
				<div class="five wide field">
					<label for="">Current Password</label>
				</div>
				<div class="eleven wide field">
					<input type="password" name="old_password">
				</div>
			</div>

			<div class="inline fields">
				<div class="five wide field">
					<label for="">Password</label>
				</div>
				<div class="eleven wide field">
					<input type="password" name="password">
				</div>
			</div>

			<div class="inline fields">
				<div class="five wide field">
					<label for="">Confirm Password</label>
				</div>
				<div class="eleven wide field">
					<input type="password" name="password_confirmation">
				</div>
			</div>

			<div class="inline fields">
				<div class="five wide field"></div>
				<div class="eleven wide field">
					<button class="ui teal button">UPDATE</button>
				</div>
			</div>
		</form>
	</div>
</div>

@endsection