@extends('client::setting.main')

@section('hicontent')
<div class="ui flat segment">
	<div class="ui top attached label">UPDATE INFORMATION PASSWORD</div>
	<div class="ui text container">
		<form action="{{ url('client/update') }}" class="ui form" method="POST">

			{!! csrf_field() !!}
				
			<div class="inline fields">
				<div class="five wide field">
					<label for="">Username</label>
				</div>
				<div class="eleven wide field">
					<input type="text" value="{{ old('username') }}" name="username">
				</div>
			</div>

			<div class="inline fields">
				<div class="five wide field">
					<label for="">Password</label>
				</div>
				<div class="eleven wide field">
					<input type="password" name="password">
				</div>
			</div>

			<div class="inline fields">
				<div class="five wide field">
					<label for="">Confirm Password</label>
				</div>
				<div class="eleven wide field">
					<input type="password" name="password_confirmation">
				</div>
			</div>

			<div class="inline fields">
				<div class="five wide field"></div>
				<div class="eleven wide field">
					<button class="ui teal button" type="submit">UPDATE</button>
				</div>
			</div>
		</form>
	</div>
</div>

@endsection