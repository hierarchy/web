<!DOCTYPE html>
<html lang="id">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>HiERARCHY WEB</title>

		<!-- Load CSS -->
		<link rel="stylesheet" href="{{ asset('assets/css/textmeone.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/semantic.min.css') }}">
		<link rel="stylesheet" href="{{ asset('vendor/client/css/offset.css') }}">
	</head>

	<body>
		<div class="ui top fixed borderless menu">
			<div class="ui container">
				<div class="item brand">
					<img src="{{ asset('assets/img/hijasa-logo.svg') }}" alt="">
					<code class="hierarchy-font">HiERARCHY</code>
				</div>
				
				<a href="" class="item">RELEASES</a>
				<a href="" class="item">DOCUMENTATION</a>
				
				<div class="right menu">
					<div class="ui dropdown item">
						<img class="ui avatar image" src="{{ Auth::user()->avatar }}">
						<i class="dropdown icon"></i>
						<div class="menu">
							<a href="{{ url('client/profile') }}" class="item">
								<i class="icon setting"></i>
								Your Setting
							</a>
							<div class="divider"></div>
							<a href="#" class="item">
								<i class="icon send"></i>
								Email Us
							</a>
							<div class="divider"></div>
							<a href="{{ url('logout') }}" class="item">
								<i class="icon power off"></i>
								Sign Out
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="pusher">
			@yield('hipage')
		</div>
		<!-- Load JS -->
		<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
		<script src="{{ asset('assets/js/semantic.min.js') }}"></script>
		<script>
			$(document).ready(function() {
				$('.ui.dropdown').dropdown();
			});
		</script>
	</body>
</html>