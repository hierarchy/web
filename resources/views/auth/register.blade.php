@extends('layouts.auth')

@section('hibody')
<div class="ui basic segment">
	<form action="{{ url('client/register') }}" class="ui form" method="POST">
		<h2 class="ui header">
			<img class="ui image" src="/assets/img/hierarchy.svg">
	  		<div class="content">Sign up to <span>HiERARCHY</span></div>
		</h2>
		
		<div class="field">
			<label for="">Please insert your information</label>
		</div>

		{!! csrf_field() !!}
		{!! Form::hidden('avatar', session('avatar')?: old('avatar')) !!}
		
		<div class="field">
			<div class="ui left icon large input">
				<i class="icon user"></i>				
				{!! Form::text('name', session('name')?: old('name'), ['placeholder' => 'Full Name']) !!}
			</div>
		</div>

		<div class="field">
			<div class="ui left icon large input">
				<i class="icon mail"></i>				
				{!! Form::text('email', session('email')?: old('email'), ['placeholder' => 'User Email']) !!}
			</div>
		</div>

		<div class="field">
			<div class="ui left icon large input">
				<i class="icon user"></i>				
				{!! Form::text('username', old('username'), ['placeholder' => 'Username']) !!}
			</div>
		</div>

		<div class="field">
			<div class="ui left icon large input">
				<i class="icon lock"></i>
				{!! Form::password('password', ['placeholder' => '*********']) !!}
			</div>
		</div>

		<div class="field">
			<div class="ui left icon large input">
				<i class="icon lock"></i>
				{!! Form::password('password_confirmation', ['placeholder' => '*********']) !!}
			</div>
		</div>

		<div class="field">
			<button type="submit" class="ui fluid large teal button">REGISTER</button>
		</div>

		<div class="ui divider"></div>

		<div class="field">
			<label for="">Already have account? <a href="{{ url('login') }}">Sign in here.</a>.</label>			
		</div>
	</form>
</div>
@endsection