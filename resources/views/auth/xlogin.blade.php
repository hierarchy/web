@extends('layouts.auth')

@section('hibody')
<div class="ui basic segment">
	<form action="" class="ui form">
		<h2 class="ui header">
			<img class="ui image" src="/assets/img/hierarchy.svg">
	  		<div class="content">Sign in to <span>HiERARCHY</span></div>
		</h2>
		
		<div class="field">
			<label for="">Already have account? Sign in here.</label>
		</div>

		<div class="field">
			<div class="ui left icon large input">
				<i class="icon user"></i>
				<input placeholder="User Email" type="text">
			</div>
		</div>

		<div class="field">
			<div class="ui left icon large input">
				<i class="icon lock"></i>
				<input placeholder="User Password" type="password">
			</div>
		</div>

		<div class="field">
			<div class="ui fluid large teal button">LOGIN</div>
		</div>

		<div class="ui horizontal divider">OR</div>

		<button class="ui fluid google plus large button">
			<i class="google plus icon"></i>
			Google Plus
		</button>

		<div class="ui divider"></div>

		<div class="field">
			<label for="">Forget password? it's ok. <a href="">Recover Here</a>.</label>
			<label for="">Dont have an account? <a href="">Register Here</a>.</label>
			
		</div>
	</form>
</div>
@endsection