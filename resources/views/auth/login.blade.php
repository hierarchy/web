@extends('layouts.auth')

@section('hibody')
<div class="ui basic segment">
	<form action="{{ url('/login') }}" class="ui form" method="POST">
	
		{!! csrf_field() !!}

		<h2 class="ui header">
			<img class="ui image" src="/assets/img/hierarchy.svg">
	  		<div class="content">Sign in to <span>HiERARCHY</span></div>
		</h2>
		
		<div class="field">
			<label for="">Already have account? Sign in here.</label>
		</div>

		<div class="field">
			<div class="ui left icon large input">
				<i class="icon user"></i>
				<input type="email" placeholder="User Email" name="email" value="{{ old('email') }}">
			</div>
		</div>

		<div class="field">
			<div class="ui left icon large input">
				<i class="icon lock"></i>
				<input placeholder="User Password" type="password" name="password">
			</div>
		</div>

		<div class="field">
			<button type="submit" class="ui fluid large teal button">LOGIN</button>
		</div>

		<div class="ui horizontal divider">OR</div>

		<a class="ui fluid google plus large button" href="{{ url('oauth/login') }}">
			<i class="google plus icon"></i>
			Google Plus
		</a>

		<div class="ui divider"></div>

		<div class="field">
			<label for="">Forget password? it's ok. <a href="">Recover Here</a>.</label>
			<label for="">Dont have an account? <a href="{{ url('register') }}">Register Here</a>.</label>
			
		</div>
	</form>
</div>
@endsection